set nocompatible
filetype off

call plug#begin('~/.local/share/nvim/plugged')
" Plugins
Plug 'Valloric/YouCompleteMe'
Plug 'Vimjas/vim-python-pep8-indent'
Plug 'bling/vim-airline'
Plug 'cespare/vim-toml'
Plug 'christoomey/vim-tmux-navigator'
Plug 'elzr/vim-json'
Plug 'fatih/vim-go'
Plug 'fisadev/vim-isort'
Plug 'janko-m/vim-test'
Plug 'junegunn/fzf'
Plug 'junegunn/vim-easy-align'
Plug 'junegunn/vim-peekaboo'
Plug 'scrooloose/nerdcommenter'
Plug 'tpope/vim-fugitive'
Plug 'vim-scripts/VimClojure'
" Themes
Plug 'joshdick/onedark.vim'
Plug 'lifepillar/vim-solarized8'
Plug 'morhetz/gruvbox'
call plug#end()

" General
filetype plugin indent on
syntax enable
let mapleader=','
set autoread
set encoding=utf-8
set history=500
set nobackup
set nowritebackup
set noswapfile

" user interface
set number        " Show line numbers
set scrolloff=2   " Set 2 lines to the cursor - when moving vertical
set ruler         " Always show current position
set cmdheight=1   " The commandbar height
set laststatus=1  " Always show statusbar
set hidden        " Change buffer - without saving
set lazyredraw  " Don't redraw while executing macros
set showmatch     " Show matching bracets when text indicator is over them
"set matchtime=2  " How many tenths of a second to blink
set cursorline    " Highlight current line
set wildmenu      " Turn on WiLd menu
set wildmode=longest:list,full
set showcmd
set shortmess+=I
set termguicolors
set inccommand=nosplit
set t_8f=[38;2;%lu;%lu;%lum
set t_8b=[48;2;%lu;%lu;%lum
"set foldmethod=syntax
autocmd QuickFixCmdPost *grep* cwindow " Open grep in quick fix

" search and replace
set ignorecase    " Ignore case when searching
set smartcase     " Override ignorecase if the pattern contains upper case
"set hlsearch      " Highlight search things
set incsearch     " Make search act like search in modern browsers
"set nowrapscan   " Turn off search wrapping
set magic         " Set magic on, for regular expressions

" indentation
set expandtab
set shiftwidth=4
set softtabstop=4
set tabstop=8
set nowrap
set linebreak
set autoindent
set cino=(0,Ws,N-s   " Align output after parantheses but not if ( is followed by whitespace
let &colorcolumn=81

" sound/signals
set noerrorbells
set novisualbell
set t_vb=
set tm=500

" backspace config
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

nmap <leader>bd :bp<bar>sp<bar>bn<bar>bd<CR>

" Move about the quickfix list more easily
nmap <leader>n :cnext<cr>
nmap <leader>N :clast<cr>
nmap <leader>p :cprev<cr>
nmap <leader>P :cfirst<cr>

autocmd QuickFixCmdPost *grep* cwindow " Open grep in quick fix
" Close completion window
autocmd CursorMovedI * if pumvisible() == 0|silent! pclose|endif
autocmd InsertLeave * if pumvisible() == 0|silent! pclose|endif

" FZF
nnoremap <silent> <C-P> :call fzf#run({'source': 'rg --files', 'sink': 'e'})<CR>
nnoremap <silent> <leader><C-P> :FZF<CR>
" Append --no-height if executed in terminal
let $FZF_DEFAULT_OPTS .= ' --no-height'

" Custom commands
:command! WhitespaceCleanup %s/\s\+$//ge | :nohl
:cmap w!! w !sudo tee > /dev/null %
nnoremap <leader>e :e <C-R>=expand("%:p:h") . "/"<CR>
nmap <leader>b :cgetfile .do.log<CR>:copen<CR>

" Airline
let g:airline_powerline_fonts=0
set laststatus=2

" YouCompleteMe
let g:ycm_enable_diagnostic_signs=0
let g:ycm_server_keep_logfiles=0
let g:ycm_collect_identifiers_from_tags_files = 1
nmap <silent> <leader>gf :YcmCompleter GoToInclude<CR>
nmap <silent> <leader>gt :YcmCompleter GoTo<CR>

" Vim-Test
let test#python#runner = 'pytest'
let test#strategy = 'neovim'
let test#python#pytest#options = ''

" Isort
let g:vim_isort_python_version = 'python3'

" Colors and fonts
set background=dark
colorscheme onedark
if has('gui_running')
    set guifont=Inconsolata\ For\ Powerline:h14
endif
