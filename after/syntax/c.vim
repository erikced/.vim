syn match    cCustomParen    "?=(" contains=cParen,cCppParen
syn match    cCustomFunc     "\w\+(\@=" contains=cCustomParen
syn match    cCustomScope    "::"
syn match    cCustomClass    "\w\+::" contains=cCustomScope
hi def link cCustomFunc  Function
hi def link cCustomClass Function
